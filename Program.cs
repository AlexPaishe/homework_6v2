﻿using System;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace FaliHellHomeWork
{
    class Program
    {
        #region Вспомогательные методы обработки массивов

        /// <summary>
        /// Метод выводящий строку с замером времени выполнения алгоритма в секундах.
        /// </summary>
        /// <param name="sv">Stopwatch</param>
        static void PrintTime(Stopwatch sv)
        {
            Console.WriteLine("Время выполнения алгоритма вычисления: " + sv.Elapsed.TotalSeconds + " сек.");
        }

        /// <summary>
        /// Вычисляем количество групп чисел, не делящихся друг на друга (степень двойк плюс единица)
        /// </summary>
        /// <param name="number">Количество чисел в последовательности от нуля</param>
        /// <returns>Возвращает количество групп чисел, не делящихся друг на друга</returns>
        static int ReturnGroupCount(int number)
        {
            int count = 1;
            while (number > 1)
            {
                number /= 2;
                count++;
            }
            return count;
        }
        #endregion

        #region Работа с файлами
        /// <summary>
        /// Проверка существования файла ввода
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        static void IsInputFile(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine($"Файл данных inputNumber.txt не найден. Создаю файл по умолчанию");
                using (StreamWriter sw = new StreamWriter(new FileStream("inputNumber.txt", FileMode.Create, FileAccess.Write)))
                {
                    sw.WriteLine("1000");
                }
            }
        }

        /// <summary>
        /// Считывание числа из файла
        /// </summary>
        /// <param name="path">Полный путь к файлу</param>
        /// <returns>Возвращает число</returns>
        static int ReadFile(string path)
        {
            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                return int.Parse(sr.ReadLine());
            }
        }

        /// <summary>
        /// Архивация файла стандартными средствами
        /// </summary>
        /// <param name="source">Исходный файл</param>
        /// <param name="output">Выходной файл архива</param>
        static void Compressig(string source, string output)
        {
            if (!File.Exists(source)) Console.WriteLine("Файл не существует! Создайте файл для архивации или проверьте правильность пути");
            else
            {
                using (FileStream fs = new FileStream(source, FileMode.Open))
                {
                    using (FileStream nf = File.Create(output))
                    {
                        using (GZipStream gs = new GZipStream(nf, CompressionMode.Compress))
                        {
                            fs.CopyTo(nf);
                        }
                    }
                }
            }
        }
        #endregion

        #region Метод выделения последовательностей
        /// <summary>
        /// Вычисляет последовательность простых чисел методом "Решето Эрастофена"
        /// </summary>
        /// <param name="list">Исходный массив битов. ненужные элементы заменябтся на true</param>
        /// <returns>Возвращает массив, где false = простые числа</returns>
        static BitArray ErastofenBool(BitArray list)
        {
            BitArray arr = new BitArray(list);
            for (int i = 2; i*2< arr.Length; i++)
            {
                if (!list[i])
                {
                    for (int j = i * 2; j < arr.Length; j += i)
                    {
                        arr[j] = true;
                    }
                }
            }
            return arr;
        }
        #endregion
        static void Main(string[] args)
        {
            string inputFile = @"inputNumber.txt";
            IsInputFile(inputFile);
            Console.WriteLine("Загружаю число из файла...");
            int number = ReadFile(inputFile);                           //Считываю txt файл
            int[][] result = new int[ReturnGroupCount(number)][];       //Массив массивов для хранения последовательностей чисел
            result[0] = new int[] { 1 };                                //Первая последовательность = 1
            Stopwatch sv = new Stopwatch();
            Console.WriteLine("Внимание! Число в файле = " + number);
            Console.WriteLine();
            Console.WriteLine("Выберите режим работы: \n" +
                "1 - Показать только кол-во групп\n" +
                "2 - Выполнить алгоритм подсчета групп чисел методом перебора значений\n" +
                "с последующей архивацией или сохранением в файл\n" +
                "Требуется мощный процессор. Миллиард не обработает\n");
            Console.WriteLine("Формирую массив чисел от 1 до вашего числа");
            switch (Console.ReadLine())
            {
                #region case "1": //Показать только кол-во групп
                case "1":
                    int[] range = new int[number - 2];
                    sv.Start();
                    Console.WriteLine("Групп чисел: " + ReturnGroupCount(number));
                    sv.Stop();
                    PrintTime(sv);
                    break;
                #endregion
                #region case "2": //Выполнить алгоритм подсчета групп чисел методом перебора значений
                case "2":
                    BitArray number_s = new BitArray(number);
                    number_s[0] = true;
                    sv.Stop();
                    PrintTime(sv);
                    Console.WriteLine();
                    Console.WriteLine("Начинаю рассчет последовательностей чисел...");
                    if (File.Exists("output3.txt"))
                    {
                        File.Delete("output3.txt");
                    }
                    sv = new Stopwatch();
                    sv.Start();
                    using (StreamWriter sw = new StreamWriter("output3.txt"))
                    {
                        sw.Write(1);
                        for (int i = 2; i < number; i++)
                        {
                            if((i & (i-1))==0)
                            {
                                sw.WriteLine();
                                sw.Write(i);
                                sw.Write(" ");
                            }
                            else
                            {
                                sw.Write(i);
                                sw.Write(" ");
                            }
                        }
                        sw.Flush();
                    }
                    sv.Stop();
                    PrintTime(sv);

                    Console.WriteLine("Производим запись результатов в файл output3.txt");
                    Console.WriteLine("Запись окончена!\n");
                    Console.WriteLine("Заархивировать полученный файл? \n" +
                       "1 - Да\n" +
                       "0 - Нет, просто выйти из программы");
                    string file = "output3.txt";
                    switch (Console.ReadLine())
                    {
                        case "1":
                            Compressig(file, "archivedOutput3.zip");
                            Console.WriteLine("Файл заархивирован. Ищите его в папке с exe файлом");
                            break;
                        case "0":
                            break;
                    }
                    break;
                #endregion
            }
        }
    }
}
